## Istio : Service Mesh
Service mesh adalah dedicated infrastruktur yang menghandle **komunikasi **service-to-service****. Mesh bertanggung jawab mengirimkan request yang reliable melalui topologi service yang kompleks.
Service mesh memiliki solusi dibagi menjadi 2 komponen :
- **Data plane** : Terdiri dari satu set intelligent proxy (Envoy) sebagai sidecar. Proxy Envoy yang akan memediasi dan mengendalikan semua komunikasi jaringan diantara microservice dengan Mixer, general-purpose policy dan telemetry hub.
- **Contol plane** : Memanage dan mengkonfigurasi proxy untuk meneruskan traffic (route traffic). Control plane juga mengkonfigurasi Mixer untuk menegakan kebijakan (enforce policies) dan mengumpulkan data telemetry (collect telemetry).

> Gambaran Flow service mesh ada di 21-istio-intro.png

## Istio Architecture
Istio adalah proyek opensource, cara kerjanya dengan menambahkan transparent layer pada aplikasi yang ada tanpa perlu melakukan perubahan pada code aplikasi. Istio merupakan platform, dan memiliki API istio, sehingga memungkinkan integrasi ke platform logging apapun atau telemetry atau policy system.

Komponen istio :

 - **Envoy** - proxy Sidecar per microservice untuk menangani lalu lintas masuk / keluar antar layanan di cluster dan dari layanan ke layanan eksternal. Proxy membentuk mesh microservice aman yang menyediakan banyak fungsi seperti discovery, routing layer-7, circuit breakers, policy enforcement, dan telemetry recording / reporting functions.
 - **Mixer** - Komponen sentral yang dimanfaatkan oleh proxy dan layanan mikro untuk menegakkan kebijakan seperti authorization, rate limits, quotas, authentication, and request tracing and collects telemetry data from the Envoy proxy and other services.
 - **Pilot** - Komponen yang bertanggung jawab untuk mengonfigurasi proxy saat runtime.
 - **Citadel** - Komponen terpusat yang bertanggung jawab untuk penerbitan dan rotasi sertifikat.
 - **Galley** - Komponen pusat untuk validating, ingesting, agregating, transforming, dan distributing config dalam Istio.
